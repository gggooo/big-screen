import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'index',
  component: () => import('../views/index.vue')
},{
  path: '/newline',
  name: 'newline',
  component: () => import('../newline/index.vue')
}]
const router = new VueRouter({
  mode: "history",
  routes
})

export default router